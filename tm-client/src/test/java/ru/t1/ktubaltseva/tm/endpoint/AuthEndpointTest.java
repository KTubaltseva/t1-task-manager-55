package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.dto.request.user.*;
import ru.t1.ktubaltseva.tm.dto.response.user.UserDisplayProfileResponse;
import ru.t1.ktubaltseva.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktubaltseva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

import static ru.t1.ktubaltseva.tm.constant.AuthTestData.*;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final static ILoggerService loggerService = new LoggerService();

    @NotNull
    private final static IPropertyService propertyService = new PropertyService();

    @NotNull
    private final static IAuthEndpoint endpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final static IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void before() throws Exception {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginResponse = endpoint.login(loginRequest);
        adminToken = loginResponse.getToken();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        userEndpoint.registry(request);
    }

    @AfterClass
    public static void after() throws AbstractException {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, USER_LOGIN);
        userEndpoint.remove(removeRequest);

        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(adminToken);
        endpoint.logout(logoutRequest);
        adminToken = null;
    }

    @Test
    public void getProfile() throws AbstractException {
        @NotNull final UserDisplayProfileResponse response = endpoint.getProfile(new UserDisplayProfileRequest(adminToken));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());
    }

    @Test
    public void login() throws Exception {
        @NotNull final UserLoginResponse response = endpoint.login(new UserLoginRequest(USER_LOGIN, USER_PASSWORD));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getToken());

        endpoint.logout(new UserLogoutRequest(response.getToken()));
    }

    @Test
    public void logout() throws Exception {
        @Nullable final String userToken = endpoint.login(new UserLoginRequest(USER_LOGIN, USER_PASSWORD)).getToken();
        @NotNull final UserLogoutResponse response = endpoint.logout(new UserLogoutRequest(userToken));
        Assert.assertNotNull(response);
    }

}
