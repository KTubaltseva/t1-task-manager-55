package ru.t1.ktubaltseva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-start-by-id";

    @NotNull
    private final String DESC = "Start task by Id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken(), id);
        getTaskEndpoint().startTaskById(request);
    }

}
