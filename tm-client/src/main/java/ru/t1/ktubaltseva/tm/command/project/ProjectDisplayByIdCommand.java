package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectDisplayByIdRequest;
import ru.t1.ktubaltseva.tm.dto.response.project.ProjectDisplayByIdResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

@Component
public final class ProjectDisplayByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-display-by-id";

    @NotNull
    private final String DESC = "Display project by Id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectDisplayByIdRequest request = new ProjectDisplayByIdRequest(getToken(), id);
        @NotNull final ProjectDisplayByIdResponse response = getProjectEndpoint().getProjectById(request);
        @Nullable final ProjectDTO project = response.getProject();
        displayProject(project);
    }

}
