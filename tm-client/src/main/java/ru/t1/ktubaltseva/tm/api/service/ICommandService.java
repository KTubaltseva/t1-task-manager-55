package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public interface ICommandService {

    void add(@Nullable AbstractCommand command);

    @NotNull
    AbstractCommand getCommandByArgument(@Nullable String argument) throws CommandNotSupportedException;

    @NotNull
    AbstractCommand getCommandByName(@Nullable String name) throws CommandNotSupportedException;

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @NotNull
    Collection<AbstractCommand> getTerminalCommandsWithArgument();

    @NotNull
    Collection<AbstractCommand> getSystemCommands();

}
