package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadJsonJaxBRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataLoadJsonJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-json-jaxb";

    @NotNull
    private final String DESC = "Load data from json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOAD JSON DATA]");
        @NotNull final DataLoadJsonJaxBRequest request = new DataLoadJsonJaxBRequest(getToken());
        getDomainEndpoint().loadDataJsonJaxB(request);
    }

}
