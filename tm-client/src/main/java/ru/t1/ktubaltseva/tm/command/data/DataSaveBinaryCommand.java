package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveBinaryRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataSaveBinaryCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-binary";

    @NotNull
    private final String DESC = "Save data to binary file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE BINARY DATA]");
        @NotNull final DataSaveBinaryRequest request = new DataSaveBinaryRequest(getToken());
        getDomainEndpoint().saveDataBinary(request);
    }

}
