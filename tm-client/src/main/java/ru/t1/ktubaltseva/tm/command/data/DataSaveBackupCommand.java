package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveBackupRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataSaveBackupCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-backup";

    @NotNull
    private final String DESC = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final DataSaveBackupRequest request = new DataSaveBackupRequest(getToken());
        getDomainEndpoint().saveDataBackup(request);
    }

}
