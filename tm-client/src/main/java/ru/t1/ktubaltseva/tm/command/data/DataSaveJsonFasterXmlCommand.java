package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveJsonFasterXmlRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataSaveJsonFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-json-faster-xml";

    @NotNull
    private final String DESC = "Save data to json file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE JSON DATA]");
        @NotNull final DataSaveJsonFasterXmlRequest request = new DataSaveJsonFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataJsonFasterXml(request);
    }

}
