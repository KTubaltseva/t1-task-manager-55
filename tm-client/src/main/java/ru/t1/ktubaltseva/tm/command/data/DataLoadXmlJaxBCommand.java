package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadXmlJaxBRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataLoadXmlJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-xml-jaxb";

    @NotNull
    private final String DESC = "Load data from xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOAD XML DATA]");
        @NotNull final DataLoadXmlJaxBRequest request = new DataLoadXmlJaxBRequest(getToken());
        getDomainEndpoint().loadDataXmlJaxB(request);
    }

}
