package ru.t1.ktubaltseva.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.api.endpoint.*;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ktubaltseva.tm.exception.system.CommandNotSupportedException;
import ru.t1.ktubaltseva.tm.util.SystemUtil;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;

@Component
public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.ktubaltseva.tm.command";

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Getter
    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (IOException e) {
            loggerService.error(e);
        }
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    private void stopFileScanner() {
        fileScanner.stop();
    }

    private void prepareStartup() {
        initPID();
        registryCommands();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        initFileScanner();
        loggerService.info("** WELCOME TO TASK MANAGER CLIENT **");
    }

    @SneakyThrows
    private void prepareShutDown() {
        loggerService.info("** TASK MANAGER CLIENT IS SHUTTING DOWN **");
        stopFileScanner();
    }

    private boolean processArgument(@Nullable final String[] args) throws ArgumentNotSupportedException, CommandNotSupportedException {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(@Nullable final String argument) throws ArgumentNotSupportedException, CommandNotSupportedException {
        if (argument == null) throw new ArgumentNotSupportedException();
        commandService.getCommandByArgument(argument);
    }

    protected void processCommand(@Nullable final String command) throws Exception {
        if (command == null) throw new CommandNotSupportedException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registryCommands() {
        for (@NotNull final AbstractCommand command : abstractCommands)
            registry(command);
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void start(@Nullable final String[] args) throws AbstractException, NoSuchAlgorithmException, IOException {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        processCommands();
    }

}
