package ru.t1.ktubaltseva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "exit";

    @NotNull
    private final String DESC = "Close Application.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
