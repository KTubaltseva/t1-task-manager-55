package ru.t1.ktubaltseva.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.api.endpoint.*;
import ru.t1.ktubaltseva.tm.api.model.ICommand;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected ILoggerService loggerService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    @Nullable
    public abstract Role[] getRoles();

    public abstract String getArgument();

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += ": " + description;
        return result;
    }

}
