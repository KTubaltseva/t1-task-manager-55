package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectDisplayListRequest;
import ru.t1.ktubaltseva.tm.dto.response.project.ProjectDisplayListResponse;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectDisplayListCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-list";

    @NotNull
    private final String DESC = "Display project list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY PROJECTS]");
        System.out.println("[ENTER SORT]:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectDisplayListRequest request = new ProjectDisplayListRequest(getToken(), sort);
        @NotNull final ProjectDisplayListResponse response = getProjectEndpoint().getAllProjects(request);
        @Nullable final List<ProjectDTO> projects = response.getProjects();
        renderProjects(projects);
    }

}
