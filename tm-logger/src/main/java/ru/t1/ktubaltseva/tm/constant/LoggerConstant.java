package ru.t1.ktubaltseva.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class LoggerConstant {

    @NotNull
    public static final String MONGO_URL = "failover://tcp://127.0.0.1:61616";

    @NotNull
    public static final String MONGO_HOST = "127.0.0.1";

    @NotNull
    public static final String MONGO_DB_NAME = "tm";

    public static final int MONGO_PORT = 27017;

    @NotNull
    public static final String QUEUE = "LOGGER";


}
