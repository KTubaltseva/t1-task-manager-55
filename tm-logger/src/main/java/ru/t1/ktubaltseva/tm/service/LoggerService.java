package ru.t1.ktubaltseva.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    @Autowired
    private MongoDatabase mongoDatabase;

    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();

        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection(table);

        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
        System.out.println(text);
    }

}
