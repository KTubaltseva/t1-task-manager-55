package ru.t1.ktubaltseva.tm.configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

import static ru.t1.ktubaltseva.tm.constant.LoggerConstant.*;

@Configuration
@ComponentScan("ru.t1.ktubaltseva.tm")
public class ApplicationConfig {

    @Bean
    public ConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory(MONGO_URL);
    }

    @Bean
    public MongoClient mongoClient() {
        return new MongoClient(MONGO_HOST, MONGO_PORT);
    }

    @Bean
    public MongoDatabase mongoDatabase(@NotNull final MongoClient mongoClient) {
        return mongoClient.getDatabase(MONGO_DB_NAME);
    }

}
