package ru.t1.ktubaltseva.tm.exception.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class ClientCallException extends AbstractException {

    public ClientCallException(@NotNull final Throwable cause) {
        super(cause);
    }

}