package ru.t1.ktubaltseva.tm.exception.auth;

public class UserIsLockedException extends AbstractAuthException {

    public UserIsLockedException() {
        super("Error! User is locked...");
    }

}