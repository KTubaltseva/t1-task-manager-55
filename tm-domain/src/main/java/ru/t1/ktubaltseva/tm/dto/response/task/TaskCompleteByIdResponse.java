package ru.t1.ktubaltseva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@NotNull final TaskDTO task) {
        super(task);
    }

}
