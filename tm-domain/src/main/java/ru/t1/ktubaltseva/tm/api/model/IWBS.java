package ru.t1.ktubaltseva.tm.api.model;

public interface IWBS extends IHasCreated, IHasName, IHasStatus, IHasDescription {
}
