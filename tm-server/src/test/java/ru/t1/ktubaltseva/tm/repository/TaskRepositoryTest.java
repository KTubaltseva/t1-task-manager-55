package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.TaskDTORepository;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.PropertyService;
import ru.t1.ktubaltseva.tm.service.dto.ProjectDTOService;
import ru.t1.ktubaltseva.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.TaskRepTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest extends AbstractRequestTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    private final ITaskDTORepository repository = new TaskDTORepository(entityManager);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        if (entityManager.getTransaction().isActive())
            entityManager.getTransaction().rollback();
        userService.add(USER_1);
        userService.add(USER_2);
        projectService.add(USER_1_PROJECT_1);
        projectService.add(USER_1_PROJECT_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        projectService.clear(USER_1.getId());
        projectService.clear(USER_2.getId());
        userService.remove(USER_1);
        userService.remove(USER_2);
        entityManager.close();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final TaskDTO task : TASK_LIST) {
            try {
                entityManager.getTransaction().begin();
                repository.removeById(task.getId());
                entityManager.getTransaction().commit();
            } catch (@NotNull final Exception e) {
                entityManager.getTransaction().rollback();
            }
        }

        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        }

        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_2.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final TaskDTO taskToAdd = USER_1_TASK_1;
        @Nullable final String taskToAddId = taskToAdd.getId();

        entityManager.getTransaction().begin();
        repository.add((taskToAdd));
        entityManager.getTransaction().commit();

        @Nullable final TaskDTO taskFindOneById = repository.findOneById(taskToAddId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskToAdd.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;

        entityManager.getTransaction().begin();
        repository.add(taskExists);
        entityManager.getTransaction().commit();

        @Nullable final TaskDTO taskFindOneById = repository.findOneById(taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());

        @Nullable final TaskDTO taskFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_TASK_ID);
        Assert.assertNull(taskFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;

        entityManager.getTransaction().begin();
        repository.add(taskExists);
        entityManager.getTransaction().commit();

        Assert.assertNull(repository.findOneById(taskExists.getUserId(), NON_EXISTENT_TASK_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, taskExists.getId()));

        @Nullable final TaskDTO taskFindOneById = repository.findOneById(taskExists.getUserId(), taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;

        entityManager.getTransaction().begin();
        repository.add(taskExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<TaskDTO> tasksFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;

        entityManager.getTransaction().begin();
        repository.add(taskExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<TaskDTO> tasksFindAllByUserRepNoEmpty = repository.findAll(taskExists.getUserId());
        Assert.assertNotNull(tasksFindAllByUserRepNoEmpty);

        @NotNull final Collection<TaskDTO> tasksFindAllByNonExistentUser = repository.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;

        entityManager.getTransaction().begin();
        repository.add(taskExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<TaskDTO> tasksFindAllByUserEmptyProject = repository.findAllByProjectId(taskExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserEmptyProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserEmptyProject);

        taskExists.setProjectId(projectExists.getId());
        @NotNull final Collection<TaskDTO> tasksFindAllByUserBindProject = repository.findAllByProjectId(taskExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserBindProject);

        @NotNull final Collection<TaskDTO> tasksFindAllByNonExistentUser = repository.findAllByProjectId(NON_EXISTENT_USER_ID, projectExists.getId());
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);

        @NotNull final Collection<TaskDTO> tasksFindAllByNonExistentProject = repository.findAllByProjectId(taskExists.getUserId(), NON_EXISTENT_PROJECT_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentProject);
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final UserDTO userToClear = USER_1;
        @NotNull final UserDTO userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final TaskDTO taskByUserToClear = USER_1_TASK_1;
        @NotNull final TaskDTO taskByUserNoClear = USER_2_TASK_1;

        entityManager.getTransaction().begin();
        repository.add(taskByUserToClear);
        repository.add(taskByUserNoClear);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.clear(userToClearId);
        entityManager.getTransaction().commit();

        @Nullable final TaskDTO taskFindOneByIdToClear = repository.findOneById(userToClearId, taskByUserToClear.getId());
        Assert.assertNull(taskFindOneByIdToClear);
        Assert.assertEquals(0, repository.findAll(userToClearId).size());

        @Nullable final TaskDTO taskFindOneByIdNoClear = repository.findOneById(userNoClearId, taskByUserNoClear.getId());
        Assert.assertEquals(taskByUserNoClear.getId(), taskFindOneByIdNoClear.getId());
        Assert.assertNotEquals(0, repository.findAll(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final TaskDTO taskToRemove = USER_1_TASK_1;

        entityManager.getTransaction().begin();
        repository.add((taskToRemove));
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.removeById(taskToRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final TaskDTO taskFindOneById = repository.findOneById(taskToRemove.getId());
        Assert.assertNull(taskFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final TaskDTO taskByUserToRemove = USER_1_TASK_1;
        @Nullable final TaskDTO taskByUserNoRemove = USER_2_TASK_1;

        entityManager.getTransaction().begin();
        repository.add((taskByUserToRemove));
        repository.add((taskByUserNoRemove));
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.removeById(userToRemoveId, taskByUserToRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final TaskDTO taskRemovedFindOneById = repository.findOneById(taskByUserToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        repository.removeById(userToRemoveId, taskByUserNoRemove.getId());
        @Nullable final TaskDTO taskNoRemovedFindOneById = repository.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void removeAllByIdByProjectId() {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final ProjectDTO projectToRemove = USER_1_PROJECT_1;
        @NotNull final ProjectDTO projectNoRemove = USER_1_PROJECT_2;
        @Nullable final TaskDTO taskByProjectToRemove = USER_1_TASK_1;
        @Nullable final TaskDTO taskByProjectNoRemove = USER_1_TASK_2;

        taskByProjectToRemove.setProjectId(projectToRemove.getId());
        taskByProjectNoRemove.setProjectId(projectNoRemove.getId());

        entityManager.getTransaction().begin();
        repository.add(taskByProjectToRemove);
        repository.add(taskByProjectNoRemove);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.removeAllByProjectId(userToRemove.getId(), projectToRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final TaskDTO taskRemovedFindOneById = repository.findOneById(userToRemove.getId(), taskByProjectToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final TaskDTO taskNoRemovedFindOneById = repository.findOneById(taskByProjectNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByProjectNoRemove.getId());
    }

}
