package ru.t1.ktubaltseva.tm.repository;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class AbstractRequestTest {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    @Nullable
    private static Database DATABASE;

    @BeforeClass
    public static void abstractBeforeClass() throws IOException, LiquibaseException, SQLException {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        @NotNull final Connection connection = getConnection(properties);
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);

        refreshLiquibase();
    }

    @AfterClass
    public static void abstractAfterClass() throws DatabaseException {
        DATABASE.close();
    }

    @NotNull
    protected static Liquibase liquibase(@NotNull final String filename) {
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }

    @NotNull
    private static Connection getConnection(@NotNull final Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

    protected static void refreshLiquibase() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changeLog/changeLog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    protected static void refreshLiquibase(@NotNull final String context) throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changeLog/changeLog-master.xml");
        liquibase.dropAll();
        liquibase.update(context);
    }

}
