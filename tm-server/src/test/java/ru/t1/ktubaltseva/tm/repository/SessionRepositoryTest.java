package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.SessionDTORepository;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.PropertyService;
import ru.t1.ktubaltseva.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest extends AbstractRequestTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final EntityManager entityManager = connectionService.getEntityManager();
    @NotNull
    private final ISessionDTORepository repository = new SessionDTORepository(entityManager);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.remove(USER_1);
        userService.remove(USER_2);
        entityManager.close();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final SessionDTO session : SESSION_LIST) {
            try {
                entityManager.getTransaction().begin();
                repository.removeById(session.getId());
                entityManager.getTransaction().commit();
            } catch (@NotNull final Exception e) {
                entityManager.getTransaction().rollback();
            }
        }

        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        }

        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_2.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final SessionDTO sessionToAdd = USER_1_SESSION_1;
        @Nullable final String sessionToAddId = sessionToAdd.getId();

        entityManager.getTransaction().begin();
        repository.add((sessionToAdd));
        entityManager.getTransaction().commit();

        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionToAddId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionToAdd.getId(), sessionFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final SessionDTO sessionToAddByUser = USER_1_SESSION_1;
        @Nullable final String sessionToAddByUserId = sessionToAddByUser.getId();

        entityManager.getTransaction().begin();
        repository.add(sessionToAddByUser);
        entityManager.getTransaction().commit();

        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionToAddByUser.getId(), sessionFindOneById.getId());

        @Nullable final SessionDTO sessionFindOneByIdByUserIdToAdd = repository.findOneById(userToAddId, sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneByIdByUserIdToAdd);
        Assert.assertEquals(sessionToAddByUser.getId(), sessionFindOneByIdByUserIdToAdd.getId());

        @Nullable final SessionDTO sessionFindOneByIdByUserIdNoAdd = repository.findOneById(userNoAddId, sessionToAddByUserId);
        Assert.assertNull(sessionFindOneByIdByUserIdNoAdd);
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;

        entityManager.getTransaction().begin();
        repository.add(sessionExists);
        entityManager.getTransaction().commit();

        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());

        @Nullable final SessionDTO sessionFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_SESSION_ID);
        Assert.assertNull(sessionFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;
        @NotNull final UserDTO userExists = USER_1;

        entityManager.getTransaction().begin();
        repository.add(sessionExists);
        entityManager.getTransaction().commit();

        Assert.assertNull(repository.findOneById(userExists.getId(), NON_EXISTENT_SESSION_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, sessionExists.getId()));

        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(userExists.getId(), sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;

        entityManager.getTransaction().begin();
        repository.clear();
        entityManager.getTransaction().commit();

        @NotNull final Collection<SessionDTO> sessionsFindAllEmpty = repository.findAll();
        Assert.assertNotNull(sessionsFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), sessionsFindAllEmpty);

        entityManager.getTransaction().begin();
        repository.add(sessionExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<SessionDTO> sessionsFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(sessionsFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final SessionDTO sessionExists = USER_1_SESSION_1;
        @NotNull final UserDTO userExists = USER_1;

        entityManager.getTransaction().begin();
        repository.add(sessionExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<SessionDTO> sessionsFindAllByUserRepNoEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(sessionsFindAllByUserRepNoEmpty);

        @NotNull final Collection<SessionDTO> sessionsFindAllByNonExistentUser = repository.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(sessionsFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), sessionsFindAllByNonExistentUser);
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final UserDTO userToClear = USER_1;
        @NotNull final UserDTO userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final SessionDTO sessionByUserToClear = USER_1_SESSION_1;
        @NotNull final SessionDTO sessionByUserNoClear = USER_2_SESSION_1;

        entityManager.getTransaction().begin();
        repository.add(USER_1_SESSION_1);
        repository.add(USER_2_SESSION_1);
        repository.clear(userToClearId);
        entityManager.getTransaction().commit();

        @Nullable final SessionDTO sessionFindOneByIdToClear = repository.findOneById(userToClearId, sessionByUserToClear.getId());
        Assert.assertNull(sessionFindOneByIdToClear);
        Assert.assertEquals(0, repository.findAll(userToClearId).size());

        @Nullable final SessionDTO sessionFindOneByIdNoClear = repository.findOneById(sessionByUserNoClear.getId());
        Assert.assertEquals(sessionByUserNoClear.getId(), sessionFindOneByIdNoClear.getId());
        Assert.assertNotEquals(0, repository.findAll(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final SessionDTO sessionToRemove = USER_1_SESSION_1;

        entityManager.getTransaction().begin();
        repository.add((sessionToRemove));

        repository.removeById(sessionToRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final SessionDTO sessionFindOneById = repository.findOneById(sessionToRemove.getId());
        Assert.assertNull(sessionFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final SessionDTO sessionByUserToRemove = USER_1_SESSION_1;
        @Nullable final SessionDTO sessionByUserNoRemove = USER_2_SESSION_1;

        entityManager.getTransaction().begin();
        repository.add((USER_1_SESSION_1));
        repository.add((USER_2_SESSION_1));

        repository.removeById(userToRemoveId, sessionByUserToRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final SessionDTO sessionRemovedFindOneById = repository.findOneById(sessionByUserToRemove.getId());
        Assert.assertNull(sessionRemovedFindOneById);

        entityManager.getTransaction().begin();
        repository.removeById(userToRemoveId, sessionByUserNoRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final SessionDTO sessionNoRemovedFindOneById = repository.findOneById(sessionByUserNoRemove.getId());
        Assert.assertNotNull(sessionNoRemovedFindOneById);
        Assert.assertEquals(sessionNoRemovedFindOneById.getId(), sessionByUserNoRemove.getId());
    }

}
