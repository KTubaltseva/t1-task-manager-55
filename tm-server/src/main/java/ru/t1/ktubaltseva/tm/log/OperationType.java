package ru.t1.ktubaltseva.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}

