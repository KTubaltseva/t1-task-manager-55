package ru.t1.ktubaltseva.tm.api.repository.dto;

import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IUserOwnedWBSDTORepository<ProjectDTO> {

}
