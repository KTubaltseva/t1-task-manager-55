package ru.t1.ktubaltseva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(
            @NotNull String userId,
            @NotNull M model
    );

    void clear(@NotNull String userId);

    boolean existsById(
            @NotNull String userId,
            @NotNull String id
    );

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(
            @NotNull String userId,
            @NotNull Comparator<M> comparator
    );

    @Nullable
    M findOneById(
            @NotNull String userId,
            @NotNull String id
    );

    int getSize(@NotNull String userId);

    void remove(
            @NotNull String userId,
            @NotNull M model
    );

    void removeById(
            @NotNull String userId,
            @NotNull String id
    );

    @Nullable
    M update(
            @NotNull String userId,
            @NotNull M model
    );

}
