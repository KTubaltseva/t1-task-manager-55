package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.model.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.model.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectTaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @Nullable final Project project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findOneById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProject(project);
            taskRepository.update(userId, resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findOneById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultTask == null) throw new TaskNotFoundException();
        return resultTask;
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            resultTask = taskRepository.findOneById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProject(null);
            taskRepository.update(userId, resultTask);
            entityManager.getTransaction().commit();
            resultTask = taskRepository.findOneById(taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultTask == null) throw new TaskNotFoundException();
        return resultTask;
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            @Nullable Project project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final List<Project> projects = projectRepository.findAll(userId);
            for (@NotNull final Project project : projects) {
                taskRepository.removeAllByProjectId(userId, project.getId());
            }
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
