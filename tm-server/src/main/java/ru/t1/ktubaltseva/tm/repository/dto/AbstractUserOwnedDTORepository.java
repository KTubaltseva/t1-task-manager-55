package ru.t1.ktubaltseva.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    public AbstractUserOwnedDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M add(
            @NotNull final String userId,
            @NotNull final M model
    ) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<M> comparator
    ) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.userId = :userId" + " " +
                "ORDER BY " + getSortColumnName(comparator);
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.id = :id and m.userId = :userId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(1) FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void remove(
            @NotNull final String userId,
            @NotNull final M model
    ) {
        Optional<M> modelResult = Optional.ofNullable(findOneById(userId, model.getId()));
        modelResult.ifPresent(this::remove);
    }

    @Override
    public void removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Nullable
    @Override
    public M update(
            @NotNull String userId,
            @NotNull M model
    ) {
        model.setUserId(userId);
        return entityManager.merge(model);
    }
}
