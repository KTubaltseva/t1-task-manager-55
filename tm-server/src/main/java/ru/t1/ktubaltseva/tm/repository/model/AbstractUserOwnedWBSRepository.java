package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.model.IUserOwnedWBSRepository;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModelWBS;
import ru.t1.ktubaltseva.tm.model.User;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedWBSRepository<M extends AbstractUserOwnedModelWBS> extends AbstractUserOwnedRepository<M> implements IUserOwnedWBSRepository<M> {

    public AbstractUserOwnedWBSRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws InstantiationException, IllegalAccessException {
        @NotNull final M model = getClazz().newInstance();
        model.setUser(entityManager.find(User.class, userId));
        model.setName(name);
        return add(userId, model);
    }

    @NotNull
    @Override
    public M create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws InstantiationException, IllegalAccessException {
        @NotNull final M model = getClazz().newInstance();
        model.setUser(entityManager.find(User.class, userId));
        model.setName(name);
        model.setDescription(description);
        return add(userId, model);
    }

}
