package ru.t1.ktubaltseva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.enumerated.Role;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @NotNull
    UserDTO create(
            @NotNull String login,
            @NotNull String password
    ) throws Exception;

    @NotNull
    UserDTO create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    UserDTO create(
            @NotNull String login,
            @NotNull String password,
            @NotNull Role role
    ) throws Exception;

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExists(@NotNull String login);

    @NotNull
    Boolean isEmailExists(@NotNull String email);

}
