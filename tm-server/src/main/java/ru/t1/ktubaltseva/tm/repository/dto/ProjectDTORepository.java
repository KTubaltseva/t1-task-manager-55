package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.comparator.NameComparator;
import ru.t1.ktubaltseva.tm.comparator.StatusComparator;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class ProjectDTORepository extends AbstractUserOwnedWBSDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

}
