package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.model.IUserOwnedService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;
import java.util.*;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultModel = modelRepository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public Collection<M> add(
            @Nullable final String userId,
            @Nullable final Collection<M> models
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<M> resultModels = new ArrayList<>();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull final M model : models) {
                @NotNull final M resultModel = modelRepository.add(userId, model);
                resultModels.add(resultModel);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModels;
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            modelRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        try {
            findOneById(userId, id);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @Nullable List<M> resultModels;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            resultModels = modelRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return resultModels;
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (comparator == null) return findAll(userId);
        @Nullable List<M> resultModels;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            resultModels = modelRepository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
        return resultModels;
    }

    @NotNull
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            resultModel = modelRepository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final EntityManager entityManager = getEntityManager();
        int result = 0;
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            result = modelRepository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            modelRepository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R modelRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            modelRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M update(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(userId, model.getId())) throw new ProjectNotFoundException();
        return update(model);
    }

}
